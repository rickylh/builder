#include <iostream>

typedef struct _carro {
    float preco;
    std::string dscMotor;
    int anoDeFabricacao;
    std::string modelo;
    std::string montadora;
} Carro;

class CarroBuilder {
protected:
	Carro* carro;

public:
	CarroBuilder() {
		carro = new Carro;
	}
	virtual void buildPreco() = 0;
	virtual void buildDscMotor() = 0;
	virtual void buildAnoDeFabricacao() = 0;
	virtual void buildModelo() = 0;
	virtual void buildMontadora() = 0;
	Carro* getCarro() {
    	return carro;
	}
};

class FiatBuilder: public CarroBuilder {
public: 
    void buildPreco() {
        // Operação complexa. 
        carro->preco = 25000.00;
    }
    void buildDscMotor() {
        // Operação complexa.
        carro->dscMotor = "Fire Flex 1.0";
    }
    void buildAnoDeFabricacao() {
        // Operação complexa.
        carro->anoDeFabricacao = 2011;
    }
    void buildModelo() {
        // Operação complexa.
        carro->modelo = "Palio";
    }
    void buildMontadora() {
        // Operação complexa.
        carro->montadora = "Fiat";
    }
};

class VolksBuilder: public CarroBuilder {
public: 
    void buildPreco() {
        // Operação complexa. 
        carro->preco = 185000.00;
    }
    void buildDscMotor() {
        // Operação complexa.
        carro->dscMotor = " V6 3.0 TDI turbodiesel";
    }
    void buildAnoDeFabricacao() {
        // Operação complexa.
        carro->anoDeFabricacao = 2018;
    }
    void buildModelo() {
        // Operação complexa.
        carro->modelo = "Amarok";
    }
    void buildMontadora() {
        // Operação complexa.
        carro->montadora = "Volkswagen";
    }
};

class ConcessionariaDirector {
private:
	 CarroBuilder* montadora;

public: 
    ConcessionariaDirector(CarroBuilder* montadora) {
        this->montadora = montadora;
    }
    ~ConcessionariaDirector() {
        delete montadora;
    }
    void construirCarro() {
        montadora->buildPreco();
        montadora->buildAnoDeFabricacao();
        montadora->buildDscMotor();
        montadora->buildModelo();
        montadora->buildMontadora();
    }
    Carro* getCarro() {
        return (montadora->getCarro());
    }
};

int main() {
	ConcessionariaDirector concessionaria_fiat ( new FiatBuilder);
	concessionaria_fiat.construirCarro();
	Carro* palio = concessionaria_fiat.getCarro();

	ConcessionariaDirector concessionaria_volks ( new VolksBuilder);
	concessionaria_volks.construirCarro();
	Carro* amarok = concessionaria_volks.getCarro();

	std::cout << "Palio: " << palio->modelo << "/";
    std::cout << palio->montadora << std::endl;
	std::cout << "Ano: " << palio->anoDeFabricacao << std::endl;
    std::cout << "Motor: " << palio->dscMotor << std::endl;
    std::cout << "Valor: " << palio->preco << std::endl << std::endl;
	std::cout << "Amarok: " << amarok->modelo << "/";
    std::cout << amarok->montadora << std::endl;
	std::cout << "Ano: " << amarok->anoDeFabricacao << std::endl;
    std::cout << "Motor: " << amarok->dscMotor << std::endl;
    std::cout << "Valor: " << amarok->preco << std::endl;

    delete palio;
    delete amarok;
	return ( 0 );
}
